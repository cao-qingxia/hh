# 移动Web第三天-课后练习

###1. 选择题

1. 以下代码如何开启弹性盒子（）？

   A.  display: none;

   B.  display: block;

   C.  display: flex;

   D.  display: inline-block;

   <details>
     <summary>参考答案</summary>
     <pre> C【解析】A. 隐藏。 B. 显示或转换成块级元素。 C. 正确。D. 转换成行内块元素</pre>
   </details>

2. 以下代码如何设置弹性盒子主轴的对齐方式（）？

   A.  align-items

   B. justify-content

   C. align-content

   D. justify-items

   <details>
     <summary>参考答案</summary>
     <pre> B【解析】A. 侧轴对齐方式。 B. 正确。 C. 行的对齐。D. 无此属性</pre>
   </details>

3. 以下代码如何设置弹性盒子侧轴的对齐方式（）？

   A.  align-items

   B. justify-content

   C. align-content

   D. justify-items

   <details>
     <summary>参考答案</summary>
     <pre> A【解析】A. 侧轴对齐方式。 B. 主轴对齐方式。 C. 行的对齐。D. 无此属性</pre>
   </details>

4. 以下代码如何单独设置弹性盒子侧轴的对齐方式（）？

   A.  align-items

   B. justify-content

   C. align-content

   D. align-self

   <details>
     <summary>参考答案</summary>
     <pre> D【解析】A. 侧轴对齐方式。 B. 主轴对齐方式。 C. 行的对齐。D. 正确</pre>
   </details>

5. 以下代码如何单独设置弹性盒子伸缩比（）？

   A.  display

   B. flex

   C. align

   D. justify

   <details>
     <summary>参考答案</summary>
     <pre> B【解析】A、C、D 属性错误。 B. 正确。</pre>
   </details>



### 2. 代码题

####2.1 两头固定，中间灵活布局

##### 2.1.1 练习背景

​		案例的实际应用场景是利用flex获取剩余宽度，所用的知识点是flex布局

##### 2.1.2 功能描述

​		1. 使用flex布局方式，不允许使用margin、padding等方式实现，不允许使用定位实现

##### 2.1.3 最终效果

![](./assets/day03/job1.png)

#####2.1.4 参考步骤：

1. 首先外面套个大盒子；设置宽、高并且开启flex布局
2. 子盒子结构基本分为三部分左侧、中间、右侧
3. 利用flex属性占据剩余空间即可

html代码：

```html
<div class="main">
    <div class="box1">
        左侧
    </div>
    <div class="box2">
        中间
    </div>
    <div class="box3">
        右侧
    </div>
</div>
```

css代码：

```css
.main {
    width: 100%;
    height: 40px;
    background: #999;
    display: flex;
    color: #fff;
    line-height: 40px;
    text-align: center;
}

.main .box1,
.main .box3 {
    width: 100px;
    height: 40px;
    background: pink;
}

.main .box2 {
    flex: 1;
    height: 100%;
    background: blue;
}
```



####2.2 按比例划分页面

##### 2.2.1 练习背景

​		案例的实际应用场景是利用flex按比例划分，所用的知识点是flex布局

##### 2.2.2 功能描述

		1. 使用flex布局方式，不允许使用margin、padding等方式实现，不允许使用定位实现
		2. 利用flex比例划分完成案例

##### 2.2.3 最终效果

![](./assets/day03/job2.png)

#####2.1.4 参考步骤：

1. 首先外面套个大盒子；设置宽、高并且开启flex布局
2. 子盒子结构基本分为三部分左侧、中间、右侧


html代码：

```html
<div class="main">
    <div class="box1">
        左侧
    </div>
    <div class="box2">
        中间
    </div>
    <div class="box3">
        右侧
    </div>
</div>
```

css代码：

```css
.main {
    width: 100%;
    height: 40px;
    background: #999;
    display: flex;
    color: #fff;
    line-height: 40px;
    text-align: center;
}
.main .box1{
    flex: 1;
    background: red;
}
.main .box2 {
    flex: 2;
    background: green;
}
.main .box3 {
    flex: 3;
    background: blue;
}
```



####2.3 携程网布局

##### 2.3.1 练习背景

​		案例的实际应用场景是flex灵活布局，所用的知识点是flex布局

##### 2.3.2 功能描述

  		1. 利用flex主轴、侧轴、单轴的各个属性完成案例
  		2. 使用flex布局方式
  		3. 顶部搜索栏是固定定位，内部为弹性布局
  		4. 导航模块中景点玩乐和图标可以先用标准流布局，等day04课程学习完毕后改为弹性布局

##### 2.3.3 最终效果

![](./assets/day03/job3.png)

#####2.3.4 参考步骤：

1. 引入normalize.css初始化样式
2. 引入index.css自定义样式
3. 页面布局分别顶部搜索模块、广告模块、导航模块
4. 顶部搜索模块固定定位，配合最大最小宽度，内部弹性布局
5. 导航模块开启弹性布局，主轴、侧轴分别控制到指定位置


html代码：

```html
<!-- 顶部搜索 -->
<div class="search-index">
    <div class="search">搜索:目的地/酒店/景点/航班号</div>
    <a href="#" class="user">我 的</a>
</div>
<!-- 焦点图模块 -->
<div class="focus">
    <img src="upload/focus.jpg" alt="">
</div>
<!-- 局部导航栏 -->
<ul class="local-nav">
    <li>
        <a href="#" title="景点·玩乐">
            <span class="local-nav-icon-icon1"></span>
            <span>景点·玩乐</span>
        </a>
    </li>
    <li>
        <a href="#" title="景点·玩乐">
            <span class="local-nav-icon-icon2"></span>
            <span>景点·玩乐</span>
        </a>
    </li>
    <li>
        <a href="#" title="景点·玩乐">
            <span class="local-nav-icon-icon3"></span>
            <span>景点·玩乐</span>
        </a>
    </li>
    <li>
        <a href="#" title="景点·玩乐">
            <span class="local-nav-icon-icon4"></span>
            <span>景点·玩乐</span>
        </a>
    </li>
    <li>
        <a href="#" title="景点·玩乐">
            <span class="local-nav-icon-icon5"></span>
            <span>景点·玩乐</span>
        </a>
    </li>
</ul>
```

css代码：

```css
body {
    max-width: 540px;
    min-width: 320px;
    margin: 0 auto;
    font: normal 14px/1.5 Tahoma, "Lucida Grande", Verdana, "Microsoft Yahei", STXihei, hei;
    color: #000;
    background: #f2f2f2;
    overflow-x: hidden;
    -webkit-tap-highlight-color: transparent;
}

ul {
    list-style: none;
    margin: 0;
    padding: 0;
}

a {
    text-decoration: none;
    color: #222;
}

div {
    box-sizing: border-box;
}


/* 搜索模块 */

.search-index {
    display: flex;
    /* 固定定位跟父级没有关系 它以屏幕为准 */
    position: fixed;
    top: 0;
    left: 50%;
    /* 固定的盒子应该有宽度 */
    -webkit-transform: translateX(-50%);
    transform: translateX(-50%);
    width: 100%;
    min-width: 320px;
    max-width: 540px;
    height: 44px;
    /* background-color: pink; */
    background-color: #F6F6F6;
    border-top: 1px solid #ccc;
    border-bottom: 1px solid #ccc;
}

.search {
    position: relative;
    height: 26px;
    line-height: 24px;
    border: 1px solid #ccc;
    flex: 1;
    font-size: 12px;
    color: #666;
    margin: 7px 10px;
    padding-left: 25px;
    border-radius: 5px;
    box-shadow: 0 2px 4px rgba(0, 0, 0, .2);
}

.search::before {
    content: "";
    position: absolute;
    top: 5px;
    left: 5px;
    width: 15px;
    height: 15px;
    background: url(../images/sprite.png) no-repeat -59px -279px;
    background-size: 104px auto;
}

.user {
    width: 44px;
    height: 44px;
    /* background-color: purple; */
    font-size: 12px;
    text-align: center;
    color: #2eaae0;
}

.user::before {
    content: "";
    display: block;
    width: 23px;
    height: 23px;
    background: url(../images/sprite.png) no-repeat -59px -194px;
    background-size: 104px auto;
    margin: 4px auto -2px;
}


/* focus */

.focus {
    padding-top: 44px;
}

.focus img {
    width: 100%;
}


/* local-nav */

.local-nav {
    display: flex;
    height: 64px;
    margin: 3px 4px;
    background-color: #fff;
    border-radius: 8px;
}

.local-nav li {
    flex: 1;
    text-align: center;
}

.local-nav a {
    font-size: 12px;
}

.local-nav li [class^="local-nav-icon"] {
    display: block;
    margin: 0 auto;
    width: 32px;
    height: 32px;
    margin-top: 8px;
    background: url(../images/localnav_bg.png) no-repeat 0 0;
    background-size: 32px auto;
}

.local-nav li .local-nav-icon-icon2 {
    background-position: 0 -32px;
}

.local-nav li .local-nav-icon-icon3 {
    background-position: 0 -64px;
}

.local-nav li .local-nav-icon-icon4 {
    background-position: 0 -96px;
}

.local-nav li .local-nav-icon-icon5 {
    background-position: 0 -128px;
}
```
