# 移动Web第二天-课后练习

### 1. 选择题

1. css属性中哪一个是动画属性？

   A.  anima

   B. animation

   C. animatoin

   D. animated

   <details>
     <summary>参考答案</summary>
     <pre> B【解析】A、C、D 错误属性。 B. 正确。</pre>
   </details>

2. 动画属性中哪两个是必须写的属性？（多选）

   A.  动画名称

   B. 延迟时间

   C. 速度曲线

   D. 动画时间

   <details>
     <summary>参考答案</summary>
     <pre> AD【解析】A、D 动画的必写属性。 B、C 可以省略。</pre>
   </details>



### 2. 代码题

####2.1 《阴阳师》二维码

##### 2.1.1 练习背景

​		案例的实际应用场景是页面动画效果，所用的知识点是css3动画属性

##### 2.1.2 功能描述

​		利用CSS3动画属性，模拟扫描二维码的场景

##### 2.1.3 最终效果

​		要求使用动画完成图例效果

![](./assets/day02/job1.gif)

#####2.1.4 参考步骤：

1. 创建index文件，新建css文件夹，创建base.css文件并初始化基本代码，新建images文件夹

2. 创建index.css保存至css文件夹，首页html文件引入css样式

3. 编写结构代码

html代码：

```html
<div class="v-code">
    <img class="erweima" src="images/57b280b496dee47507111c56NRN73rVj.png" alt="">
    <img class="line" src="images/line_dd0b705.png" alt="">
</div>
```

css代码：

```css

body {
    padding: 50px;
}
.v-code {
    width: 145px;
    height: 240px;
    padding-top: 47px;
    float: left;
    position: relative;
    background: url(../images/index_z_71df05e.png);
    background-position: 0px 0px;
    background-repeat: no-repeat;
}
.v-code .erweima {
    display: block;
    margin: 0 auto;
    width: 107px;
}
.v-code .line {
    position: absolute;
    left: 12px;
    top: 45px;
    -webkit-animation: mainbarcode 4s linear infinite;
    -moz-animation: mainbarcode 4s linear infinite;
    -ms-animation: mainbarcode 4s linear infinite;
    animation: mainbarcode 4s linear infinite;
}
@keyframes mainbarcode {
    0% {
        -webkit-transform: translate3d(0,0,0);
        -moz-transform: translate3d(0,0,0);
        -ms-transform: translate3d(0,0,0);
        transform: translate3d(0,0,0);
    }
    50% {
        -webkit-transform: translate3d(0,100px,0);
        -moz-transform: translate3d(0,100px,0);
        -ms-transform: translate3d(0,100px,0);
        transform: translate3d(0,100px,0);
    }
    100% {
        -webkit-transform: translate3d(0,0,0);
        -moz-transform: translate3d(0,0,0);
        -ms-transform: translate3d(0,0,0);
        transform: translate3d(0,0,0);
    }
}
```



####2.2 《战意》小动画

##### 2.2.1 练习背景

​		案例的实际应用场景是页面动画效果，所用的知识点是过渡属性

##### 2.2.2 功能描述

​		1. 页面的整体背景必须是一屏显示

​		2. 鼠标移入就停止动画

##### 2.2.3 最终效果

​		要求使用过渡属性完成图例效果

​		![](./assets/day02/job2.gif)

#####2.2.4 参考步骤：

1. 创建基本结构
2. 设置基本样式和鼠标经过样式
3. 设置鼠标经过样式


html代码：

```html
<div class="play-video"></div>
```

css代码：

```css

html, body {
    height: 100%;
    background: url(../images/bg_81ab3d6.jpg) no-repeat center top;
}

.play-video {
    position: absolute;
    top: 280px;
    right: 380px;
    width: 93px;
    height: 93px;
    margin-left: -47px;
    z-index: 4;
}

.play-video::before,
.play-video::after {
    display: block;
    width: 100%;
    height: 100%;
    content: "";
    position: absolute;
}

.play-video::before {
    background-image: url(../images/index_z_9769688.png);
    background-position: -744px -819px;
    background-repeat: no-repeat;
    -webkit-animation: rotate1 2s linear infinite 300ms 0 ease;
    -moz-animation: rotate1 2s linear infinite 300ms 0 ease;
    -ms-animation: rotate1 2s linear infinite 300ms 0 ease;
    animation: rotate1 2s linear infinite 300ms 0 ease;
    -moz-animation: rotate1 2s linear infinite;
    -webkit-animation: rotate1 2s linear infinite;
    -o-animation: rotate1 2s linear infinite;
    -ms-animation: rotate1 2s linear infinite;
    animation: rotate1 2s linear infinite;
}

.play-video::after {
    position: absolute;
    left: 50%;
    top: 50%;
    width: 21px;
    height: 25px;
    margin-top: -12px;
    margin-left: -10px;
    background-image: url(../images/index_z_9769688.png);
    background-position: -2290px -107px;
    background-repeat: no-repeat;
}

@keyframes rotate1 {
    0% {
        transform: rotate(0deg);
    }
    100% {
        transform: rotate(360deg);
    }
}

.play-video:hover {
    cursor: pointer;
}

.play-video:hover::before {
    -webkit-animation-play-state: paused;
    -moz-animation-play-state: paused;
    -o-animation-play-state: paused;
    animation-play-state: paused;
    background-repeat: no-repeat;
}
```

css鼠标移上去代码：

```css
.product li:hover {
    -webkit-box-shadow: 0 15px 30px rgba(0, 0, 0, 0.1);
    box-shadow: 0 15px 30px rgba(0, 0, 0, 0.1);
    -webkit-transform: translate3d(0, -2px, 0);
    transform: translate3d(0, -2px, 0);
    /*这里的位移可以使用2d的转换方式*/
}

.product li:hover .review {
    opacity: 1;
    height: 76px;
}
```



####2.3 《阴阳师》f4

##### 2.3.1 练习背景

​		案例的实际应用场景是页面动画，所用的知识点是动画和多个动画组合

##### 2.3.2 功能描述

​		当浏览此网页的时候，如果停顿等待的时间较长，将会出现4个小人物走动，滚动或者鼠标经过就消失

##### 2.3.3 最终效果

​		要求使用动画完成图例效果

​		![](./assets/day02/job3.gif)

#####2.3.4 参考步骤：

1. 先进行基本的布局，排出来具体的大小位置，在底部均匀分布
2. 给每个盒子设置背景，然后创建公共移动的动画
3. 创建该变背景的动画，将两个动画引入在不同盒子上，重复执行


html代码：

```html
<div class="f4_box">
    <div class="walk01">
        <div class="west-01"></div>
    </div>
    <div class="walk02">
        <div class="west-02"></div>
    </div>
    <div class="walk03">
        <div class="west-03"></div>
    </div>
    <div class="walk04">
        <div class="west-04"></div>
    </div>
</div>
```

css代码：

```css

.f4_box {
    position: fixed;
    width: 100%;
    height: 175px;
    left: 0;
    bottom: 0;
    z-index: 99;
    -webkit-transition: all 3.2s ease;
    -moz-transition: all 3.2s ease;
    -ms-transition: all 3.2s ease;
    transition: all 3.2s ease;
}
.walk01 {
    position: absolute;
    left: 0;
    bottom: 0;
    width: 154px;
    height: 121px;
    opacity: 0;
    -webkit-animation: move01 15s linear infinite;
    -moz-animation: move01 15s linear infinite;
    -ms-animation: move01 15s linear infinite;
    animation: move01 15s linear infinite;
}
.west-01 {
    width: 154px;
    height: 121px;
    background-image: url(../images/txgc_6d0e9e5.png);
    background-repeat: no-repeat;
    -webkit-animation: play01 .4s steps(2) infinite;
    -moz-animation: play01 .4s steps(2) infinite;
    -ms-animation: play01 .4s steps(2) infinite;
    animation: play01 .4s steps(2) infinite;
}
@keyframes move01 {
    0% {
        -webkit-transform: translate(-200px,0);
        -moz-transform: translate(-200px,0);
        -ms-transform: translate(-200px,0);
        transform: translate(-200px,0);
        opacity: 1;
    }
    100% {
        -webkit-transform: translate(1920px,0);
        -moz-transform: translate(1920px,0);
        -ms-transform: translate(1920px,0);
        transform: translate(1920px,0);
        opacity: 1;
    }
}
@keyframes play01 {
    0% {
        background-position: 0 0;
    }
    100% {
    background-position: -308px 0;
    }
}
.walk02 {
    position: absolute;
    left: 0;
    bottom: 0;
    width: 167px;
    height: 176px;
    opacity: 0;
    -webkit-animation: move01 15s linear 2s infinite;
    -moz-animation: move01 15s linear 2s infinite;
    -ms-animation: move01 15s linear 2s infinite;
    animation: move01 15s linear 2s infinite;
}
.west-02 {
    width: 167px;
    height: 176px;
    background-image: url(../images/txgh_fee2457.png);
    background-repeat: no-repeat;
    -webkit-animation: play02 .4s steps(2) infinite;
    -moz-animation: play02 .4s steps(2) infinite;
    -ms-animation: play02 .4s steps(2) infinite;
    animation: play02 .4s steps(2) infinite;
}
@keyframes play02 {
    0% {
        background-position: 0 0;
    }
    100% {
        background-position: -334px 0;
    }
}

.walk03 {
    position: absolute;
    left: 0;
    bottom: 0;
    width: 161px;
    height: 149px;
    opacity: 0;
    -webkit-animation: move01 15s linear 4s infinite;
    -moz-animation: move01 15s linear 4s infinite;
    -ms-animation: move01 15s linear 4s infinite;
    animation: move01 15s linear 4s infinite;
}
.west-03 {
    width: 161px;
    height: 149px;
    background-image: url(../images/txgl_c15cfd8.png);
    background-repeat: no-repeat;
    -webkit-animation: play03 .4s steps(2) infinite;
    -moz-animation: play03 .4s steps(2) infinite;
    -ms-animation: play03 .4s steps(2) infinite;
    animation: play03 .4s steps(2) infinite;
}
@keyframes play03 {
    0% {
        background-position: 0 0;
    }
    100% {
    background-position: -322px 0;
    }
}
.walk04 {
    position: absolute;
    left: 0;
    bottom: 0;
    width: 171px;
    height: 157px;
    opacity: 0;
    -webkit-animation: move01 15s linear 6s infinite;
    -moz-animation: move01 15s linear 6s infinite;
    -ms-animation: move01 15s linear 6s infinite;
    animation: move01 15s linear 6s infinite;
}
.west-04 {
    width: 171px;
    height: 157px;
    background-image: url(../images/txgq_d281c9b.png);
    background-repeat: no-repeat;
    -webkit-animation: play04 .4s steps(2) infinite;
    -moz-animation: play04 .4s steps(2) infinite;
    -ms-animation: play04 .4s steps(2) infinite;
    animation: play04 .4s steps(2) infinite;
}
@keyframes play04 {
    0% {
        background-position: 0 0;
    }
    100% {
        background-position: -342px 0;
    }
}
```



####2.4 《战意》小动画

##### 2.4.1 练习背景

​		案例的实际应用场景是页面动画效果，所用的知识点是使用动画属性结合其他属性一起使用

##### 2.4.2 功能描述

		1. 以往的背景经过都是半透明盒子遮罩直上直下或者渐入渐出
		2. 以斜着进入离开需要用到CSS3的斜切

##### 2.4.3 最终效果

​		![](./assets/day02/job4.gif)

#####2.2.4 参考步骤：

3. 保存图片，新建文件，初始化样式：
2. 设置鼠标经过样式，利用放大的斜切的原理，让父盒子溢出隐藏


html代码：

```html
<div class="content">
    <a href="javascipt:;"></a>
</div>
```

css代码：

```css
* {
    margin: 0;
    padding: 0;
}
.content {
    width: 275px;
    height: 185px;
    background: url(bgi.jpg) no-repeat;
    margin: 100px auto;
    overflow: hidden;
}
a {
    display: block;
    width: 100%;
    height: 100%;
    background-color: rgba(47, 214, 133, .75);
    opacity: 0;
    transition: all .5 ease-in;
}
.content:hover a {
    opacity: 1;
    animation: donghua .3s forwards;
}

@keyframes donghua {
    from {
        transform: translateX(130%) skewX(-45deg) scale(1);
    }
    to {
        transform: translateX(0) skewX(-45deg) scale(2);
    }
}
```

