# 移动Web第四天-课后练习

### 1. 选择题

1. 以下代码如何修改弹性盒子主轴方向（）？

   A.  felx-direction

   B. flex-direction

   C. flex-wrap

   D. flex-item

   <details>
     <summary>参考答案</summary>
     <pre> B【解析】A、C、D 错误。 B. 正确。</pre>
   </details>

2. 以下代码如何设置弹性盒子的换行（）？

   A.  felx-direction

   B. flex-direction

   C. flex-wrap

   D. flex-item

   <details>
     <summary>参考答案</summary>
     <pre> C【解析】A、B、D 错误。 C. 正确。</pre>
   </details>

3. 以下代码如何设置弹性盒子行的对齐方式（）？

   A.  felx-direction

   B. flex-direction

   C. flex-wrap

   D. align-content

   <details>
     <summary>参考答案</summary>
     <pre> D【解析】A、B、C 错误。 D. 正确。</pre>
   </details>



### 2. 代码题

####2.1  画骰子

##### 2.1.1 练习背景

​		案例的实际应用场景是flex布局，所用的知识点是flex布局

##### 2.1.2 功能描述

		1. 使用flex布局方式
		2. 不允许使用margin、padding等方式实现
		3. 不允许使用定位实现

##### 2.1.3 最终效果

​		![](./assets/day04/job4.png)



##### 2.1.4 参考步骤：

1. 首先外面套个大盒子；设置宽、高并且开启flex布局（桌布）
2. 大盒子中，需要放置6个小盒子：设置宽、高并且开启flex布局（骰子）
3. 每一个小盒子里面放span，个数分别是1、2、3、4、5、6


html代码：

```html
<ul>
    <li><span></span></li>
    <li><span></span><span></span></li>
    <li><span></span><span></span><span></span></li>
    <li><span></span><span></span><span></span><span></span></li>
    <li><span></span><span></span><span></span><span></span><span></span></li>
    <li><span></span><span></span><span></span><span></span><span></span><span></span></li>
</ul>
```

css代码：

```css
* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}
ul {
    list-style: none;
    width: 500px;
    height: 500px;
    margin: 100px auto;
    background: green;
    border: 2px solid black;
    border-radius: 10px;
    display: flex;
    flex-wrap: wrap;
    padding: 50px;
    justify-content: space-evenly;
    align-content: space-evenly;
}
li {
    width: 100px;
    height: 100px;
    padding: 10px;
    border-radius: 10px;
    background: white;
    border: 1px solid black;
    display: flex;
}
li>span{
    width: 25px;
    height: 25px;
    border-radius: 50%;
    background: black;
}
li:nth-child(1){
    justify-content: center;
    align-items: center;
}
li:nth-child(1)>span{
    background: red;
}
li:nth-child(2),li:nth-child(3){
    justify-content: space-evenly;
    align-items: center;
}
li:nth-child(3)>span:nth-child(1){
    align-self: flex-start;
}
li:nth-child(3)>span:nth-child(3){
    align-self: flex-end;
}
li:nth-child(4){
    box-sizing: border-box;
    padding: 5px;
    flex-wrap: wrap;
    justify-content: space-evenly;
    align-items: center;
}
li:nth-child(4)>span{
    margin: 5px;
}
li:nth-child(5){
    box-sizing: border-box;
    padding: 5px;
    flex-wrap: wrap;
    justify-content: space-between;
    align-content: center;
}
li:nth-child(5)>span{
    margin: 3px 5px;
}
li:nth-child(5)>span:nth-child(3){
    margin: 0 31px;
}
li:nth-child(6){
    flex-wrap: wrap;
    justify-content: space-evenly;
    align-content: space-around;
}
```



####2.2  小兔鲜儿移动端-订单页面

##### 2.2.1 练习背景

​		案例的实际应用场景是flex布局，所用的知识点是flex布局

##### 2.2.2 功能描述

  		1. 使用flex布局方式
  		2. 不允许使用margin、padding等方式实现
  		3. 不允许使用定位实现

##### 2.2.3 最终效果	

​	和视频教材中的案例效果保持一致

##### 2.2.4 参考步骤：

​	结合视频完成案例（最好能够独立完成）

​	详细步骤情参考视频教材



####2.3  小兔鲜儿pc端-个人中心

##### 2.3.1 练习背景

​		案例的实际应用场景是flex布局，所用的知识点是flex布局

##### 2.3.2 功能描述

  		1. 使用flex布局方式
  		2. 不允许使用margin、padding等方式实现
  		3. 不允许使用定位实现

##### 2.3.3 最终效果	

​	和视频教材中的案例效果保持一致

##### 2.3.4 参考步骤：

​	结合视频完成案例（最好能够独立完成）

​	详细步骤情参考视频教材

