# 移动Web第一天-课后练习

### 1. 选择题

1. 平面转换中的位移应该用以下哪个属性值？

   A . transition

   B. translate

   C. scale

   D. rotate	

   <details>
     <summary>参考答案</summary>
     <pre> B【解析】A. 过渡。 B. 位移。 C. 缩放。D. 旋转</pre>
   </details>

2. 平面转换中的旋转应该用以下哪个属性值？

   A . transition

   B. translate

   C. scale

   D. rotate	

   <details>
     <summary>参考答案</summary>
     <pre> D【解析】A. 过渡。 B. 位移。 C. 缩放。D. 旋转</pre>
   </details>

3. 平面转换中的缩放应该用以下哪个属性值？

   A . scale

   B. translate

   C. transform

   D. rotate	

   <details>
     <summary>参考答案</summary>
     <pre> A【解析】A. 正确。 B. 位移。 C. 转换。D. 旋转</pre>
   </details>

4. 渐变的背景应该用以下哪个属性值？

   A . linear-gredient

   B. linear-gradient

   C. linaer-gradient

   D. linear-gradeint

   <details>   <summary>参考答案</summary>   <pre>【解析】B。其他答案单词错误</pre></details>



### 2. 代码题

####2.1 字体图标快捷导航

##### 2.1.1 练习背景

​		案例的实际应用场景是字体图标的使用，所用的知识点是字体图标

##### 2.1.2 功能描述

​		熟练使用字体图标，请按照阿里巴巴字体图标的font class方式来使用字体图标，要求：字体图标有颜色、字体大小

##### 2.1.3 最终效果

![](./assets/day01/job01.png)

#####2.1.4 参考步骤：

1. 引入字体图标
2. 搭建页面结构
3. 放入字体图标
4. 改变字体图标样式

html代码：

```html
<ul class="nav">
    <li><a href="#" class="red"><i class="iconfont icon-shouye"></i>主页</a></li>
    <li><a href="#" class="orange"><i class="iconfont icon-fenlei"></i>分类</a></li>
    <li><a href="#" class="yellow"><i class="iconfont icon-dingdan"></i>订单</a></li>
    <li><a href="#" class="green"><i class="iconfont icon-huiyuan"></i>会员</a></li>
    <li><a href="#" class="cyan"><i class="iconfont icon-wode"></i>我的</a></li>
    <li><a href="#" class="blue"><i class="iconfont icon-kefu"></i>客服</a></li>
    <li><a href="#" class="purple"><i class="iconfont icon-changyonggoupiaorenshanchu"></i>删除</a></li>
</ul>
```

引入地址代码：

```html
<link rel="stylesheet" href="./iconfont/iconfont.css">
```

css代码：

```css
* {
    margin: 0;
    padding: 0;
}

li {
    list-style: none;
}

a {
    text-decoration: none;
}

.nav {
    width: 200px;
    margin: 100px auto;
    border: 1px solid black;
    border-bottom: none;
}

.nav li {
    border-bottom: 1px solid black;
}

.nav li a,
.nav li i {
    font-size: 20px;
    line-height: 30px;
}

.nav li a {
    padding: 0 10px;
}

.nav li i {
    padding-right: 10px;
}

.red {
    color: red;
}

.orange {
    color: orange;
}

.yellow {
    color: yellow;
}

.green {
    color: green;
}

.cyan {
    color: cyan;
}

.blue {
    color: blue;
}

.purple {
    color: purple;
}
```



####2.2 《小米》产品

##### 2.2.1 练习背景

​		案例的实际应用场景是过渡的使用，所用的知识点是过渡

##### 2.2.2 功能描述

​		这个产品模块中，鼠标经过大盒子，底部有信息模块升上来，并且本身上移2像素，并且加上投影

##### 2.2.3 最终效果

![](./assets/day01/job02.gif)

**案例素材**：

![](./assets/day01/job2-sucai.jpg)

#####2.1.4 参考步骤：

1. 先进行基本的布局，排出来具体的大小位置

2. 添加底部信息块，，定位在bottom底部，完成后，将高度设置为0，加上溢出隐藏属性

3. 当鼠标经过盒子，显示盒子，并且加上阴影和位移

   

   html代码：

```html
<div class="product">
    <ul>
        <li>
            <div class="pro-img">
                <a href="#">
                    <img src="images/pms_1524883847.49276938!220x220.jpg" alt="">
                </a>
            </div>
            <h3><a href="#">小米电视4A 43英寸青春版</a></h3>
            <p class="desc">全高清屏 / 人工智能语音</p>
            <p class="price">
                <span>1499</span>元
                <del>1699</del>
            </p>
            <div class="review">
                <a href="#">
                    <span class="msg">一如既往的好，小米情怀</span>
                    <span class="auther"> 来自于 惊喜 的评价 </span>
                </a>
            </div>
        </li>
    </ul>
</div>
```

css代码：

```css
body {
    padding: 100px;
    background-color: #f5f5f5;
}

.product li {
    float: left;
    width: 234px;
    height: 246px;
    padding: 34px 0 20px;
    z-index: 1;
    margin-left: 14px;
    margin-bottom: 14px;
    background: #fff;
    -webkit-transition: all .2s linear;
    transition: all .2s linear;
    position: relative;
}

.pro-img {
    width: 150px;
    height: 150px;
    margin: 0 auto 18px;
}

.pro-img a {
    width: 100%;
    height: 100%;
}

.pro-img img {
    display: block;
    width: 100%;
    height: 100%;
}

.product li h3 {
    margin: 0 10px;
    font-size: 14px;
    font-weight: 400;
    text-align: center;
}

.product li h3 a {
    color: #333;
}

.desc {
    margin: 0 10px 10px;
    height: 18px;
    font-size: 12px;
    text-align: center;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    color: #b0b0b0;
}

.price {
    font-size: 14px;
    margin: 0 10px 10px;
    text-align: center;
    color: #ff6700;
}

.price del {
    color: #b0b0b0;
}

.review {
    position: absolute;
    bottom: 0;
    left: 0;
    z-index: 3;
    width: 234px;
    height: 0;
    overflow: hidden;
    font-size: 12px;
    background: #ff6700;
    opacity: 0;
    -webkit-transition: all .2s linear;
    transition: all .2s linear;
}

.review a {
    color: #757575;
    display: block;
    padding: 8px 30px;
    outline: 0;
}

.review a span {
    display: block;
    margin-bottom: 5px;
    color: #fff;
}
```

css鼠标移上去代码：

```css
.product li:hover {
    -webkit-box-shadow: 0 15px 30px rgba(0, 0, 0, 0.1);
    box-shadow: 0 15px 30px rgba(0, 0, 0, 0.1);
    -webkit-transform: translate3d(0, -2px, 0);
    transform: translate3d(0, -2px, 0);
    /*这里的位移可以使用2d的转换方式*/
}

.product li:hover .review {
    opacity: 1;
    height: 76px;
}
```



####2.3 《beats耳机》按钮

##### 2.3.1 练习背景

​		案例的实际应用场景是过渡的使用，所用的知识点是过渡

##### 2.3.2 功能描述

​		当鼠标经过按钮的时候，有一个灰色的背景或者其他颜色的背景升上来

##### 2.3.3 最终效果

![](./assets/day01/job03.gif)



#####2.3.4 参考方案

1. 创建基本结构

2. 设置基本样式和鼠标经过样式

   html代码：

```html
<ul class="buttons clearfix">
    <li>
        <a href="#">
            <span class="button-inner">探索</span>
        </a>
    </li>
</ul>
```

css代码：

```css
.buttons {
    width: 500px;
    margin: 100px auto;
}

.buttons li {
    float: left;
    margin: 10px;
}

.buttons li a {
    position: relative;
    display: block;
    width: 100px;
    text-align: center;
    line-height: 40px;
    color: #1e1e1e;
    border: 2px solid #d6d6d6;
    border-radius: 20px;
    box-sizing: border-box;
    overflow: hidden;
}

.buttons li a::after {
    content: '';
    position: absolute;
    background-color: #d6d6d6;
    width: 100%;
    height: 0%;
    left: 0;
    bottom: 0;
    transition: all 0.2s ease-in;
}

.buttons li:hover a::after {
    height: 100%;
}

.buttons .button-inner {
    position: relative;
    z-index: 2;
}
```

