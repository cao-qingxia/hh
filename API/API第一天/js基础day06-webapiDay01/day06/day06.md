# 复习

1. 立即执行函数有几种写法，怎么写？

(function(){

}());

(function(){

})();

2. 如何创建一个对象，对象中的成员有哪些？

let obj = {
    // 键值对
    // 对象中的成员就是 属性和方法
    name:'jack',
    hello:function(){

    }
}

3. 访问对象的属性有哪几种方式？有什么区别？

A. 小圆点形式
    对象.属性名
    对象.属性名 = 值

B. 关联数组形式
    对象['属性名']
    对象[ 变量 ] = 值

区别：
小圆点形式访问对象的属性时， 属性名是固定不变的
关联数组形式访问对象的属性的时候，可以给一个变量，由变量来决定要访问的数据是谁。

4. 如何添加/修改对象的成员？如何删除对象中的成员？

对象.属性名 = 值
对象.方法名 = function(){}

delete 对象.属性名

5. 如何遍历一个对象中保存的所有数据？

for(let k in obj){

    console.log(obj.k)


    console.log(obj[ k ])
}

# js基础第六天 

数据类型的内存分配原理：

js的数据类型分类：
A. 简单数据类型(基本数据类型，值类型)：数字，字符串，布尔值，undefined
B. 复杂数据类型(引用数据类型，引用类型)：数组，对象

这些数据在进行保存的时候，会将之保存到 内存当中，内存当中划分有 栈，堆 两个区域。
不同的数据类型在内存中存储的区域是不一样的。
存储的区域不一样，会有不一样的处理方式和做法
简单类型的数据会存储在 栈 区域中。
复杂类型的数据会存储在 堆 区域中。

# web apis 课程第一天
整个课程是要带着大家学习一系列的操作网页或者浏览器的 对象或方法

DOM（Document Object Model——文档对象模型）：将网页中的元素当做对象一样来使用

## 1. 获取一个元素的 querySelector 方法

// querySelector方法是document对象中的一个方法，这个方法有一个参数，参数是需要找到的元素的 选择器字符串
// querySelector方法会找到第一个匹配 选择器字符串的元素并返回， 如果找不到呢,返回值就是null
let box = document.querySelector('.box')

## 2. 获取多个元素的 querySelectorAll 方法

// querySelectorAll方法会根据参数 选择器字符串到页面中查找符合要求的元素
// 并且将所有元素放在一个 伪数组(NodeList) 中，并返回
// 什么是伪数组？跟数组一样有元素索引，有长度length，但是没有数组方法的对象就是 伪数组
let ones = document.querySelectorAll('.one')

## 3. 设置文字内容 的 innerText, 设置 html源码的 innerHTML

// dom对象的 innerText属性 可以获取或修改元素的文字内容
one.innerText = '<p>呵呵哒</p>'

// dom对象的 innerHTML属性 可以获取或修改元素的 内部html代码
ul.innerHTML = `<li>酱爆犹豫</li>
                <li>一鸡两吃</li>
                <li>红烧臭豆腐</li>`

innerHTML 和 innerText的区别
innerHTML 会解析和渲染 标签元素
innerText 是纯文本，不会解析和渲染标签元素。

## 4. 修改元素的属性

dom对象.属性名 = 值

## 5. 修改元素的css样式

A. 通过style属性进行修改

dom对象.style.css属性 = 值

注意事项：
1.修改样式通过style属性引出
2.如果属性有-连接符，需要转换为小驼峰命名法
3.赋值的时候，需要的时候不要忘记加css单位(px,vwvh,rem)

B. 通过 className 设置类名样式(旧)

dom对象.className = '类名1 类名2'

注意：
className 属性因为是赋值，所以会覆盖之前的类名
className 属性也有兼容性问题。

C. 通过 classList 来进行类名 添加，删除，切换等操作

dom对象.classList.add('类名') // 这个add方法可以在元素中添加类名(原本的类名依然存在，不会覆盖)
dom对象.classList.remove('类名') // 将元素中的类名删除
dom对象.classList.toggle('类名') // 让类名在添加和删除之间切换(有类名就删除，没有类名就添加、)

# 总结

1. 简单数据类型存储在 内存中的栈区域
   复杂数据类型存储在 内存中的堆区域

   结论：简单类型的数据之间互相没有关联，一个变量的修改不会影响到另外一个变量的值。
         保存复杂数据类型数据的变量，如果保存的是同一个对象的地址，一个变量修改之后，另一个变量的值也会发生变化

2. 获取元素【重点】

    document.querySelector() // 获取一个元素
    document.querySelectorAll()  // 获取的是多个元素，并以 NodeList 形式的伪数组返回

    其他方法【了解】：
    document.getElementById() //根据id获取一个元素
    document.getElementsByTagName()  // 根据标签名称获取所有的标签(多个元素), 返回的是伪数组
    document.getElementsByClassName()  // 根据类名获取多个元素，返回也是伪数组
    document.getElementsByName()  // 根据name属性来进行获取元素，返回也是伪数组

3. 修改元素的内容【重要】

    dom对象.innerText = '文本内容，不会渲染标签'
    dom对象.innerHTML = 'html源代码，会渲染标签'

4. 修改元素的属性
    // 修改属性的值
    dom对象.属性名 = 值
    // 获取属性的值
    dom对象.属性名

5. 修改元素样式的方式【重要】

    A. 通过style属性进行修改
        // 设置和修改的是行内样式
        dom对象.style.属性名 = 值

注意事项：
1.修改样式通过style属性引出
2.如果属性有-连接符，需要转换为小驼峰命名法
3.赋值的时候，需要的时候不要忘记加css单位(px,vwvh,rem)

    B. 通过className属性进行类名修改【不推荐】
        // className 属性会赋值覆盖之前的类名
        dom对象.className = '类名1 类名2'

    C. 通过classList进行类名添加，删除，切换【推荐】

        dom对象.classList.add('类名')
        dom对象.classList.remove('类名')
        dom对象.classList.toggle('类名')